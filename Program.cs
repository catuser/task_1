﻿using System;
using MyMenuSpace;
using MyGameSpace;

namespace Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            bool IsExit = false;
            string UserName;
            MyMenu ConsoleMenu = new MyMenu();

            Console.Clear();
            UserName = ConsoleMenu.WriteName();

            while(!IsExit)
            {
                int ChooseButton = ConsoleMenu.ChooseMenu();

                switch(ChooseButton)
                {
                    case 0:
                    {
                        MyGame ConsoleGame = new MyGame();
                        ConsoleGame.InfoGame();
                        ConsoleGame.StartGame(UserName);
                        break;
                    }
                    case 1:
                    {
                        UserName = ConsoleMenu.WriteName();
                        break;
                    }
                    case 2:
                    {
                        Console.WriteLine($"{UserName}, До Свидание.");
                        Console.WriteLine("Нажмите любую клавишу, чтобы закрыть окно.");
                        Console.ReadLine();
                        IsExit = true;
                        break;
                    }
                }
            }
        }
    }
}
