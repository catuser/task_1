using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace MyMenuSpace
{
    class MyMenu
    {
        public void DrawMenu(int selectButton)
        {
            Console.Clear();
            if(selectButton == 0)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Start");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Change Name");
                Console.WriteLine("Exit");
            }
            if(selectButton == 1)
            {
                Console.WriteLine("Start");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Change Name");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Exit");
            }
            if(selectButton == 2)
            {
                Console.WriteLine("Start");
                Console.WriteLine("Change Name");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Exit");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public string WriteName()
        {
            Console.Write("Введите ваше имя: ");
            string UserName = Console.ReadLine();
            return UserName;
        }

        public int ChooseMenu()
        {
            Console.Clear();
            int ChooseButton = 0;
            DrawMenu(ChooseButton);
            while(true)
            {
                var ch = Console.ReadKey(false).Key;
                switch(ch)
                {
                    case ConsoleKey.UpArrow:
                    {
                       Console.WriteLine("Вверх!!!");

                       if(ChooseButton == 0)
                       {
                           ChooseButton = 2;
                           DrawMenu(ChooseButton);
                           continue;
                       }
                       ChooseButton--;
                       DrawMenu(ChooseButton);
                       break;
                    }
                    case ConsoleKey.DownArrow:
                    {
                       Console.WriteLine("Вниз!!!");

                       if(ChooseButton == 2)
                       {
                           ChooseButton = 0;
                           DrawMenu(ChooseButton);
                           continue;
                       }
                       ChooseButton++;
                       DrawMenu(ChooseButton);
                       break;
                    }
                    case ConsoleKey.Enter:
                    {
                        Console.Clear();
                        return ChooseButton;
                    }
                }
            }
        }
    }
}