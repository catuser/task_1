using System;

namespace MyGameSpace
{
    class MyGame
    {
        public int RandomNumber;
        public MyGame()
        {
            var Rand = new Random();
            RandomNumber = Rand.Next(0, 100);
        }

        public void InfoGame()
        {
            Console.WriteLine("Правила игры:");
            Console.WriteLine("Нужно отгадать загаданное число от 0 до 100.");
            Console.WriteLine("Необходимо вводить свое предположение, после чего нажать на 'Enter'");
            Console.WriteLine("После чего получите ответ.");
            Console.WriteLine("Всего у вас будет семь попыток.");
            Console.WriteLine("Можно вводить только цифры.");
            Console.WriteLine("_____________________________________________________________________");
            Console.WriteLine("Нажмите любую клавишу, чтобы начать.");
            Console.ReadLine();
            Console.Clear();
        }

        public void StartGame(string userName)
        {
            Console.WriteLine("Начали!!!");
            
            for(int i = 0; i < 7; i++)
            {
                int EnterNumber;

                Console.WriteLine($"\nПопытка {i+1})");
                Console.Write("Введите число: ");
                string EnterStringNumber = Console.ReadLine();
                if(Int32.TryParse(EnterStringNumber, out EnterNumber))
                {
                    bool is_win = CheckNumber(EnterNumber, userName);
                    if(is_win)
                    {
                        Console.WriteLine($"Вы отгадали число с {i+1} попытки.");
                        Console.WriteLine("Нажмите любую клавишу, чтобы выйти в меню.");
                        Console.ReadLine();
                        return;
                    }
                }
                else
                {
                    Console.WriteLine("Неправильный ввод. Попробуйте снова.");
                    i--;
                }

                if(i == 6)
                {
                    Console.WriteLine("\nУпс, вы проиграли.");
                    Console.WriteLine("Нажмите любую клавишу, чтобы выйти в меню.");
                    Console.ReadLine();
                }
            }
        }

        public bool CheckNumber(int enterNumber, string userName)
        {
            if(enterNumber == RandomNumber)
            {
                Console.WriteLine($"\nПоздравляю вас {userName}, вы выиграли!!!");
                return true;
            }
            if(enterNumber > RandomNumber)
            {
                Console.WriteLine($"{userName}, загаданное число меньше.");
            }
            if(enterNumber < RandomNumber)
            {
                Console.WriteLine($"{userName}, загаданое число больше.");
            }
            return false;
        }
    }
}